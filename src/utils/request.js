import axios from 'axios';
import { Message } from 'element-ui';
import { removeToken } from '@/utils/auth';
import { createRouter } from '@/router';

const showToast = msg => {
  Message({
    message: msg || 'Error',
    type: 'error',
    duration: 5 * 1000,
  });
};

// 创建 axios 实例
const axiosService = axios.create({
  // 跨域携带 cookies
  withCredentials: true,
  // 超时时间 5 秒
  timeout: 5000,
});

// 请求拦截
axiosService.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    // 打印日志，便于查看请求的错误信息
    console.log(`请求拦截：${error}`);
    return Promise.reject(error);
  },
);

// 响应拦截
axiosService.interceptors.response.use(
  response => {
    const res = response.data;
    // 登录失效，重置 Token
    if (typeof res === 'string' && res.indexOf('html') !== -1) {
      removeToken();
      createRouter();
      location.reload();
      return Promise.reject(new Error(res.message || 'Error'));
    } else {
      // 登录未失效，返回接口信息
      return res;
    }
  },
  error => {
    // 打印日志，便于查看响应的错误信息
    console.log(`响应拦截：${error}`);
    showToast(error.message);
    return Promise.reject(error);
  },
);

// 导出
export default axiosService;
