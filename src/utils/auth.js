/**
 * Created by wangyxt on 2020-05-14
 * 操作 Token，用于判断用户的登录状态
 */

import Cookies from 'js-cookie';

// 存储在 Cookie 中的 key
const tokenKey = 'life-user-name';

// 将信息存储到 Cookie 中
export function setToken(token) {
  return Cookies.set(tokenKey, token);
}

// 从 Cookie 中获取对应的信息
export function getToken() {
  return Cookies.get(tokenKey);
}

// 从 Cookie 中移除对应的信息
export function removeToken() {
  return Cookies.remove(tokenKey);
}
