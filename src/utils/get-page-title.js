/**
 * Created by wangyxt on 2020-05-17
 * 获取页面标题
 */

import defaultSettings from '@/settings';

const title = defaultSettings.title || '宇通生活商家管理平台';

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`;
  }
  return `${title}`;
}
