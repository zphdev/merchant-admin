/**
 * Created by wangyxt on 2020-05-07
 * 使用密钥为密码加密
 */

String.prototype.replaceAll = function(find, rep) {
  const exp = new RegExp(find, 'g');
  return this.replace(exp, rep);
};

const encryptByDES = (message, key) => {
  const keyHex = CryptoJS.enc.Utf8.parse(key);
  const encrypted = CryptoJS.DES.encrypt(message, keyHex, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  });
  let str = encrypted.toString();
  str = str.replaceAll('\\+', '[j]');
  str = str.replaceAll('/', '[x]');
  str = str.replaceAll('=', '[d]');
  str = str.replaceAll(' ', '');
  return str;
};

export default encryptByDES;
