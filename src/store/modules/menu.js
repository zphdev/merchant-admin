/**
 * Created by wangyxt on 2020-05-17
 */

import router from '@/router';
import routes from '@/router/routes';
import axiosService from '@/utils/request';
import { menusApi } from '@/store/api';

const state = {
  menus: [],
};

const mutations = {
  SET_MENUS: (state, menus) => {
    state.menus = menus;
  },
};

const actions = {
  setMenus({ commit }) {
    return new Promise((resolve, reject) => {
      // 获取用户菜单
      axiosService({
        method: 'POST',
        url: menusApi,
      })
        .then(data => {
          console.log(data, '--------');
          commit('SET_MENUS', routes);
          router.addRoutes(routes);
          resolve();
        })
        .catch(err => reject(err));
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
