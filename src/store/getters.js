const getters = {
  sidebar: state => state.app.sidebar,
  menus: state => state.menu.menus,
};

export default getters;
