import getRealUrl from '@/utils/get-real-url';

export const menusApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/index/menus',
});
