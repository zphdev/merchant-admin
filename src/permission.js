/**
 * Created by wangyxt on 2020-05-14
 * 全局权限校验
 */

// 引入路由和进度条
import router from '@/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
// 获取 Token
import { getToken } from '@/utils/auth';
import store from '@/store';
import getPageTitle from '@/utils/get-page-title';

// 配置进度条
NProgress.configure({ showSpinner: false });

// 配置路由白名单，白名单路由不会重定向
const whiteList = ['/login', '/forget'];

// 路由开始之前
router.beforeEach(async (to, from, next) => {
  // 开始显示进度条
  NProgress.start();

  // 设置页面标题
  document.title = getPageTitle(to.meta.title);

  // 获取 Token
  const hasToken = getToken();

  // 如果没有 Token
  if (!hasToken) {
    // 在路由白名单中，直接跳转；否则重定向到登录页面
    if (whiteList.indexOf(to.path) !== -1) {
      next();
    } else {
      next(`/login?redirect=${to.path}`);
      NProgress.done();
    }
  }

  // 如果存在 Token
  if (hasToken) {
    // 目标地址是登录页，直接重定向到首页
    if (to.path === '/login') {
      next({ path: '/' });
      NProgress.done();
    }

    // 目标地址不是登录页，直接跳转到对应的页面
    if (to.path !== '/login') {
      const hasMenu = store.getters.menus && store.getters.menus.length > 0;
      if (hasMenu) {
        next();
      } else {
        await store.dispatch('menu/setMenus');
        next({ ...to, replace: true });
      }
    }
  }
});

// 路由结束停止进度条
router.afterEach(() => NProgress.done());
