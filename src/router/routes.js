export default [
  // 商品管理
  {
    path: '/goods-management/goods-list',
    name: 'GoodsManagement',
    component: () => import('@/views/goods-list/goods-list'),
  },

  // 门票及旅游管理
  {
    path: '/management/list',
    name: 'TicketList',
    component: () => import('@/views/ticket-list/ticket-list'),
  },

  // 服务管理
  {
    path: '/management',
    component: () => import('@/layout/layout'),
    redirect: 'noRedirect',
    meta: {
      title: '服务管理',
      icon: '404',
    },
    alwaysShow: true,
    children: [
      {
        path: 'ticket-list',
        component: () => import('@/views/ticket-list/ticket-list'),
        name: 'TicketList',
        meta: { title: '玩乐门票', icon: 'dashboard' },
      },
    ],
  },

  // 订单管理
  {
    path: '/order',
    component: () => import('@/layout/layout'),
    redirect: 'noRedirect',
    meta: {
      title: '订单管理',
      icon: '404',
    },
    alwaysShow: true,
    children: [
      {
        path: 'order-management',
        component: () => import('@/views/order-management/order-management'),
        name: 'OrderManagement',
        meta: { title: '电商订单', icon: 'dashboard' },
      },
      {
        path: 'order-after-sale',
        component: () => import('@/views/order-after-sale/order-after-sale'),
        name: 'OrderManagement',
        meta: { title: '售后服务单', icon: 'dashboard' },
      },
    ],
  },

  // 404 页面
];
