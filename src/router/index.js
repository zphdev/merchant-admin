import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

// 基础路由，所有的用户都可以加载
const baseRoutes = [
  // 登录
  {
    path: '/login',
    component: () => import('@/views/login/login'),
    hidden: true,
  },

  // 忘记密码
  {
    path: '/forget',
    component: () => import('@/views/forget/forget'),
    hidden: true,
  },

  // 首页
  {
    path: '/',
    component: () => import('@/layout/layout'),
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/home'),
        name: 'Home',
        meta: { title: '首页', icon: 'dashboard' },
      },
    ],
  },
];

// 创建路由
export const createRouter = () => {
  return new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes: baseRoutes,
  });
};

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  // reset router
  router.matcher = newRouter.matcher;
}

export default router;
