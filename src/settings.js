/**
 * Created by wangyxt on 2020-05-14
 * 全局设置信息
 */

module.exports = {
  /**
   * @type {string} 项目标题
   */
  title: '宇通生活商家管理平台',
};
