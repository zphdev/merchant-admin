import getRealUrl from '@/utils/get-real-url';

export const logoutApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/logout',
});
