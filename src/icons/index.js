import Vue from 'vue';
import SvgIcon from '@/components/svg-icon/svg-icon.vue';

// 注册到全局
Vue.component('svg-icon', SvgIcon);

const requireAll = requireContext => requireContext.keys().map(requireContext);
// 创建模块的上下文，具体说明可参考https://juejin.im/post/5ab8bcdb6fb9a028b77acdbd#heading-2
const req = require.context('./svgs', false, /\.svg$/);
requireAll(req);
