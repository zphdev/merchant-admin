import getRealUrl from '@/utils/get-real-url';

export const loginApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/login',
});

export const imgValidateCodeApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/imgValidateCode',
});

export const genDesKeyApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/genDesKey',
});
