import getRealUrl from '@/utils/get-real-url';

export const smsValidateCodeApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/smsValidateCode',
});

export const resetPasswordApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/resetPassword',
});

export const genDesKeyApi = getRealUrl({
  mockId: '44',
  apiUrl: '/serve/genDesKey',
});
