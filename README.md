使用 Element-UI 和 Vue 重写的商家后台。

环境：

```
node >= 11.10.1
npm >= 6.14.0
```

## 安装依赖

```bash
$ npm install
```

## 启动开发环境

```bash
$ npm run serve
```

## 灰度环境打包

```bash
$ npm run gray
```

## 生产环境打包

```bash
$ npm run build
```

## 单元测试

```bash
$ npm run test:unit
```

## 代码检查并修复

```bash
$ npm run lint
```

## 生成模板文件

使用 `plop` 生成模板文件。

```bash
$ npm run new
```
